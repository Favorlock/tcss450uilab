package group8.tcss450.uw.edu.uilab;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import group8.tcss450.uw.edu.uilab.dummy.DummyContent;


/**
 * A simple {@link Fragment} subclass.
 */
public class ViewStudentFragment extends Fragment {


    public ViewStudentFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_student, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        Bundle args = getArguments();

        if (args != null) {
            if (args.containsKey("id")) {
                TextView tv = getActivity().findViewById(R.id.studentId);
                tv.setText(args.getString("id"));
            }

            if (args.containsKey("name")) {
                TextView tv = getActivity().findViewById(R.id.studentName);
                tv.setText(args.getString("name"));
            }

            if (args.containsKey("details")) {
                TextView tv = getActivity().findViewById(R.id.studentDetails);
                tv.setText(args.getString("details"));
            }
        }
    }
}
