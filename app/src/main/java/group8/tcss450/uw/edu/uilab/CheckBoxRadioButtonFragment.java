package group8.tcss450.uw.edu.uilab;


import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class CheckBoxRadioButtonFragment extends Fragment {


    public CheckBoxRadioButtonFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_check_box_radio_button, container, false);
        RadioButton rb = view.findViewById(R.id.radioYes);
        rb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRadioButtonClicked(v);
            }
        });

        rb = view.findViewById(R.id.radioNo);
        rb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRadioButtonClicked(v);
            }
        });

        Button b = view.findViewById(R.id.submit);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSubmit(v);
            }
        });

        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
        GradientDrawable bg = (GradientDrawable) getActivity().findViewById(R.id.radioGroup).getBackground();
        bg.setColor(getResources().getColor(R.color.fill));
    }

    public void onSubmit(View view) {
        RadioButton radioYes = getActivity().findViewById(R.id.radioYes);
        RadioButton radioNo = getActivity().findViewById(R.id.radioNo);

        CheckBox checkboxMeat = getActivity().findViewById(R.id.checkboxMeat);
        CheckBox checkboxCheese = getActivity().findViewById(R.id.checkboxCheese);
        CheckBox checkboxVeges = getActivity().findViewById(R.id.checkboxVegetables);
        CheckBox checkboxSauce = getActivity().findViewById(R.id.checkboxSauce);

        StringBuilder sb = new StringBuilder();

        if (radioYes.isChecked()) {
            sb.append(" yes,");
        }

        if (radioNo.isChecked()) {
            sb.append(" no,");
        }

        if (checkboxMeat.isChecked()) {
            sb.append(" meat,");
        }

        if (checkboxCheese.isChecked()) {
            sb.append(" cheese,");
        }

        if (checkboxVeges.isChecked()) {
            sb.append(" vegetables,");
        }

        if (checkboxSauce.isChecked()) {
            sb.append(" sauce,");
        }

        if (sb.length() > 0) {
            sb.setLength(sb.length() - 1);
        }

        Toast toast = Toast.makeText(getContext(), "You selected: " + sb.toString(), Toast.LENGTH_SHORT);
        toast.show();
    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        GradientDrawable bg = (GradientDrawable) getActivity().findViewById(R.id.radioGroup).getBackground();

        switch (view.getId()) {
            case R.id.radioYes:
                if (checked)
                    bg.setColor(getResources().getColor(R.color.yes));
                break;
            case R.id.radioNo:
                if (checked)
                    bg.setColor(getResources().getColor(R.color.no));
                break;
        }
    }

}
